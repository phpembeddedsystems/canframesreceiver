# My own Can Case / Can reader

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Simple can receiver to dumping can frames.

# New Features!

  - No new features


You can also:
  - Read CAN frames

### Hardware

Lista modółów potrzebnych do budowy projektu:

* Arduino Nano V3 - head unit
* MCP2515 - stand-alone CAN controller with SPI interface [Datasheet](https://github.com/SeeedDocument/CAN_BUS_Shield/raw/master/resource/MCP2515.pdf)
* XL4015 - Simple DC/DC Step-down converter
* OBD2 plug - optional

### Wiring

OBDII interface
![OBDII interface](img/OBD.png)

Arduino Nano connecting to MCP2515
![CanBusSchema](img/mb_canbus_schema.png)

To do

### Todos

 - Write wiring
 - Add source codes

License
----

MIT


**Free Software, Hell Yeah!**